
function User(id,nickname) {
    return{
        nickname: nickname,
        id : id
    }
}
function Room(id_r,name_r,per){
    return {
        id : id_r,
        name : name_r,
        list_Users : [],
        list_Admin : [],
        list_Member : [],
        Per : per,
        Add_Member : function(Member_){
            this.list_Users.push(Member_);
            this.list_Member.push(Member_);
        },
        Add_Admin: function(Admin_){
            this.list_Users.push(Admin_);
            this.list_Admin.push(Admin_);
        },
        Add_User: function(User_){
            this.list_Users.push(User_);
        },
        put_hands: function(){
            if(this.Per==true){
                let i;
                for(i=0;i<this.list_Users.length;i++){
                    this.list_Users[i].Per=true;  
                }
                for(i=0;i<this.list_Member.length;i++){
                    this.list_Member[i].Per=true;  
                }
             }
            for (let u in this.list_Users){
                console.log(`${u} ${this.list_Users[u].Rise_Hand()}`);
            }
    
        }
    };
}
function Admin(id,nickname) {
    return{
        nickname: nickname,
        id : id,
        Per : true,
        Rise_Hand : function(){
            let Perm="main levee";
            return (Perm);
         },
         Create_Room : function(id_r,name_r,Tab){
            let R=new Room(id_r,name_r,false);
            R.list_Users=Tab;
            R.list_Users.push(this);
            return (R);
        }
    };
}
function Member(id,nickname) {
    return{
        nickname: nickname,
        id : id,
        Rise_Hand : function(){
            let Perm;
            if(this.Per==true){ Perm="main levee" }
            else { Perm="main non levee";}
            return (Perm);
        }
    };
}
let u1=Member(0,"toto");
let u2=Member(1, "jsbro");
let u3=Member(2, "newbie");
let adm1=Admin(1,"king");
let r=adm1.Create_Room(0,"first",[u1,u2]);
let g=r;
g.Add_User(u3);
for(key in r){
console.log(key+":" +JSON.stringify(r[key]));
}
console.log("\n");
r.Per=false;
r.put_hands();
console.log("\n");
r.Per=true;
r.put_hands();



